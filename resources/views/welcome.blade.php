@extends('layout.master')

@section('judul')
    AYY, NICE!
@endsection

@section('content')
    <h1>Selamat Datang {{$namadepan}} {{$namabelakang}}!</h1>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection
