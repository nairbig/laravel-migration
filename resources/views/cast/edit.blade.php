@extends('layout.master')

@section('judul')
    Edit Cast {{$cast->nama}}
@endsection

@section('content')
    <div>
        <h2>Tambah Data</h2>
            <form action="/cast/{{$cast->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" class="form-control" value={{$cast->nama}} name="nama" id="title" placeholder="Masukkan Nama">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Umur</label>
                    <input type="number" class="form-control" value={{$cast->umur}} name="umur" id="body" placeholder="Masukkan Umur">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Biodata</label>
                    <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan biodata">{{$cast->bio}}</textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
    </div>
@endsection

